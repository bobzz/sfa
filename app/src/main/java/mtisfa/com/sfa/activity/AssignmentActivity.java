package mtisfa.com.sfa.activity;

import android.content.Intent;
import android.graphics.Color;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import mtisfa.com.sfa.R;
import mtisfa.com.sfa.Utility.MyCookieJar;
import mtisfa.com.sfa.Utility.Utility;
import mtisfa.com.sfa.activity.ListItem.AssignmentAdapter;
import mtisfa.com.sfa.model.API;
import mtisfa.com.sfa.model.Assignment;
import mtisfa.com.sfa.model.Response.AssignmentListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssignmentActivity extends AppCompatActivity {
    ListView table;
    TextView noData;
    Realm realm;
    SwipeRefreshLayout swipe;
    ProgressBar loading;
    List<Assignment> listData;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        realm.close();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment);
        realm=Realm.getDefaultInstance();
        table = (ListView) findViewById(R.id.tableAssignment);
        noData=(TextView) findViewById(R.id.noData);
        loading = (ProgressBar) findViewById(R.id.loading);
        swipe = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getAssignment();

            }
        });
        getAssignment();
    }
    public void getAssignment() {
        loading.setVisibility(View.VISIBLE);
        MyCookieJar cookieJar = Utility.utility.getCookieFromPreference(this);
        API api = Utility.utility.getAPIWithCookie(cookieJar);
        String user = Utility.utility.getLoggedName(this);
        Call<AssignmentListResponse> assignment = api.getAssignment("[[\"Assignment\",\"docstatus\",\"=\",1],[\"Assignment\",\"user\",\"=\",\""+user+"\"]]");
        assignment.enqueue(new Callback<AssignmentListResponse>() {
            @Override
            public void onResponse(Call<AssignmentListResponse> call, Response<AssignmentListResponse> response) {
                realm.beginTransaction();
                final RealmResults<Assignment> assignment_list = realm.where(Assignment.class).findAll();
                assignment_list.deleteAllFromRealm();
                if (Utility.utility.catchResponse(getApplicationContext(), response)) {
                    AssignmentListResponse result = response.body();
                    if (result.data.size()==0){
                        if (noData.getVisibility()== View.INVISIBLE){
                            noData.setVisibility(View.VISIBLE);
                            table.setVisibility(View.INVISIBLE);
                        }
                    }else{
                        noData.setVisibility(View.INVISIBLE);
                        table.setVisibility(View.VISIBLE);
                        listData=result.data;
                        realm.copyToRealm(listData);
                    }
                }
                refreshTable();
                realm.commitTransaction();
                loading.setVisibility(View.INVISIBLE);
                swipe.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<AssignmentListResponse> call, Throwable throwable) {
                Utility.utility.showConnectivityUnstable(getApplicationContext());
                loadLocal();
                loading.setVisibility(View.INVISIBLE);
                swipe.setRefreshing(false);
                refreshTable();
            }
        });
    }
    public void loadLocal(){
        realm.beginTransaction();
        listData = realm.where(Assignment.class).findAll();
        realm.commitTransaction();
    }
    AssignmentAdapter adapter;
    public void refreshTable(){
        adapter= new AssignmentAdapter(getApplicationContext(), listData);
        table.setAdapter(adapter);
    }
}
